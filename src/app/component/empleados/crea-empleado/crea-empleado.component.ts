import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormGroupDirective, NgForm, Validators, FormBuilder, MaxLengthValidator } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { EmpleadoServiceService } from '../empleado-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AreaService } from '../area.service';
import { Empleado } from '../../../model/empleado';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-crea-empleado',
  templateUrl: './crea-empleado.component.html',
  styleUrls: ['./crea-empleado.component.css']
})
export class CreaEmpleadoComponent implements OnInit {


  listAreas: any[];
  validacioNombre: "^[A-Za-z\s]+$";
  validarNumero: "^[0-9,$]*$";
  nuevoEmpleado: Empleado;
  form: FormGroup;
  constructor(
    private empleadoService: EmpleadoServiceService,
    private areaService: AreaService,
    private FormBuilder: FormBuilder,
    private router: Router,
    private activate: ActivatedRoute
  ) {

    this.form = FormBuilder.group({
      id: 0,
      p_nombre: ["", [Validators.required, Validators.maxLength(20)]],
      s_nombre: ["", [Validators.required, Validators.maxLength(20), Validators.pattern(this.validacioNombre)]],
      p_apellido: ["", [Validators.required, Validators.maxLength(20), Validators.pattern(this.validacioNombre)]],
      s_apellido: ["", [Validators.required, Validators.maxLength(20), Validators.pattern(this.validacioNombre)]],
      pais: ["", Validators.required],
      t_identificacion: [[Validators.required]],
      n_identificacion: ["", [Validators.required, Validators.pattern(this.validarNumero)]],
      f_ingreso: [""],
      area: ["", Validators.required],
    });

  }

  ngOnInit(): void {

    this.areaService.getArea().subscribe(
      (area) => {
        this.listAreas = area.areas;

      }
    )
  }

  guardar(event: Event) {

    if (this.form.valid) {

      this.nuevoEmpleado = this.form.value;
      console.log(this.form.value)
      this.empleadoService.guardarEmpleado(this.nuevoEmpleado).subscribe(
        (empleado) => {

          if (empleado.ok) {
            this.form.reset();
            Swal.fire({
              title: 'Empleado creado',
              text: 'Empleado creado con exito',
              icon: 'success',
            }).then((result) => {
              if (result) {
                this.router.navigate(['/']);
              }
            })
          }
        }, error => {

          if (error) {
            Swal.fire({
              title: "Error al crear el usuario",
              text: error.error.message,
              icon: 'warning',
            })
          }
        }
      );

    } else {
      Swal.fire({
        title: "Error!",
        text: "llene el formulario correctamente",
        icon: "warning"
      })
    }

  }

}
