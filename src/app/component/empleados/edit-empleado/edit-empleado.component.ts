import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { RouterLinkActive, Router, ActivatedRoute } from '@angular/router';
import { Empleado } from 'src/app/model/empleado';
import { EmpleadoServiceService } from '../empleado-service.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms'
import { AreaService } from '../area.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-edit-empleado',
  templateUrl: './edit-empleado.component.html',
  styleUrls: ['./edit-empleado.component.css']
})
export class EditEmpleadoComponent implements OnInit, AfterViewInit {


  @ViewChild('correo') correo;

  listAreas: any;
  id: string;
  empleado: Empleado;
  form: FormGroup;
  validacioNombre: "^[A-Za-z\s]+$";
  validarNumero: "^[0-9,$]*$";
  n_correo: any;
  n_estado: any;
  constructor(
    private areaService: AreaService,
    private FormBuilder: FormBuilder,
    private empleadoServis: EmpleadoServiceService,
    private router: ActivatedRoute,
    private route: Router
  ) {


  }

  ngAfterViewInit(): void {

  }

  ngOnInit(): void {


    this.form = this.FormBuilder.group({
      id: 0,
      p_nombre: ["", [Validators.required, Validators.maxLength(20)]],
      s_nombre: ["", [Validators.required, Validators.maxLength(20), Validators.pattern(this.validacioNombre)]],
      p_apellido: ["", [Validators.required, Validators.maxLength(20), Validators.pattern(this.validacioNombre)]],
      s_apellido: ["", [Validators.required, Validators.maxLength(20), Validators.pattern(this.validacioNombre)]],
      pais: ["", Validators.required],
      t_identificacion: [[Validators.required]],
      n_identificacion: ["", [Validators.required, Validators.pattern(this.validarNumero)]],
      f_ingreso: [""],
      area: ["", Validators.required],
      estado: new FormControl({ value: "", disabled: true }, [Validators.required]),
      correo: new FormControl({ value: "", disabled: true }, [Validators.required])
    });



    this.areaService.getArea().subscribe(area => {

      this.listAreas = area.areas;
    })

    this.router.params.subscribe(params => {

      console.log(params);
      if (params.id) {
        this.id = params.id;
        if (this.id) {
          this.empleadoServis.obtenerUnEmpleado(this.id).subscribe(
            res => {

              if (res.ok) {

                this.empleado = res.empleado;

                console.log(res.empleado)

                this.n_correo = res.empleado.correo;
                this.n_estado = res.empleado.estado;
                this.form.patchValue({
                  p_nombre: res.empleado.p_nombre,
                  s_nombre: res.empleado.s_nombre,
                  p_apellido: res.empleado.p_apellido,
                  s_apellido: res.empleado.s_apellido,
                  correo: res.empleado.correo,
                  estado: res.empleado.estado,
                  area: res.empleado.area,
                  pais: res.empleado.pais,
                  t_identificacion: res.empleado.t_identificacion,
                  n_identificacion: res.empleado.n_identificacion,
                  f_ingreso: res.empleado.fecha_ingreso,
                })
              }
            },
            error => {
              Swal.fire({
                title: "Error al actualizar empleado",
                text: "Error!",
                icon: "error"
              })
            }
          )
        }
      }
    })






  }

  guardar(event: any) {

    //console.log(this.form.value)
    //console.log(this.empleado)
    if (this.form.valid) {

      let persona = {
        p_nombre: "",
        s_nombre: "",
        p_apellido: "",
        s_apellido: "",
        area: "",
        correo: "",
        estado: "",
        f_ingreso: new Date(),
        pais: "",
        t_identificacion: "",
        n_identificacion: 0,
      }

      this.empleado = this.form.value;

      persona.p_nombre = this.empleado.p_nombre.toLocaleUpperCase();
      persona.s_nombre = this.empleado.s_nombre.toLocaleUpperCase();
      persona.correo = this.n_correo;
      persona.estado = this.n_estado;
      persona.p_apellido = this.empleado.p_apellido.toLocaleUpperCase();
      persona.s_apellido = this.empleado.s_apellido.toLocaleUpperCase();
      persona.t_identificacion = this.empleado.t_identificacion;
      persona.n_identificacion = this.empleado.n_identificacion;
      persona.pais = this.empleado.pais;
      persona.f_ingreso = this.empleado.f_ingreso
      persona.area = this.empleado.area;


      this.empleadoServis.actualizarEmpleado(this.id, persona).subscribe(

        (empleado) => {

          if (empleado.ok) {
            Swal.fire({
              title: "Actualizado!",
              text: "Correo Actualizado exitosamente",
              icon: "success"
            }).then((value) => {
              if (value) {
                this.route.navigate(['/']);
              }
            })
          }

        }, error => {
          console.log(error)
          if (error) {
            Swal.fire({
              title: 'Error!'
              , text: 'Error al actualizar el Empleado',
              icon: "error"
            })
          }
        }
      )



    } else {
      Swal.fire({
        title: '!',
        text: 'llene el formulario correctamente',
        icon: 'info'
      })
    }


  }
}
