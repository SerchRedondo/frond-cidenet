import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AreaService {

  constructor(
    private http: HttpClient
  ) { }

  getArea(): Observable<any> {
    return this.http.get(environment.URL_OPCIONES + 'area');
  }

}
