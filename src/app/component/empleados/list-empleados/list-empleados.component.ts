import { Component, OnInit, ViewChild } from '@angular/core';
import { EmpleadoServiceService } from '../empleado-service.service';
import { MatTableDataSource } from '@angular/material/table'
import { MatPaginator } from '@angular/material/paginator';
import { Empleado } from 'src/app/model/empleado';
import Swal from 'sweetalert2';

import { faUserPlus, faEdit, faUserSlash } from '@fortawesome/free-solid-svg-icons'




@Component({
  selector: 'app-list-empleados',
  templateUrl: './list-empleados.component.html',
  styleUrls: ['./list-empleados.component.css']
})
export class ListEmpleadosComponent implements OnInit {

  faUserPlus = faUserPlus;
  faUserSlash = faUserSlash;
  faEdit = faEdit;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = [
    'nro',
    'Indentificacion',
    'Nombres', 'Apellidos', 'correo',
    'estado', 'area', 'pais', 'opciones'];

  public buscarEmpleado = "";

  public dataSource: any;
  public lista_empleado: Empleado[];

  constructor(
    private empleadoService: EmpleadoServiceService
  ) { }

  ngOnInit(): void {

    this.empleadoService.obtenerEmpleados().subscribe(

      empleado => {

        this.lista_empleado = empleado.empleados;

        this.dataSource = new MatTableDataSource<any>(this.lista_empleado)

        this.dataSource.paginator = this.paginator;

      }
    )
  }

  eliminar(id: any, event: any) {

    let path = event.path[3];

    Swal.fire({
      title: "Desea Eliminar el usuario?",
      icon: 'question',
      showCancelButton: true,
      confirmButtonText: 'Si deseo Eliminarlo',
      cancelButtonText: 'Cancelar Operacion'
    }).then((value) => {
      if (value.isConfirmed) {

        this.empleadoService.eliminarEmpleado(id).subscribe(
          empleado => {

            if (empleado) {
              Swal.fire({
                title: "Usuario Eliminado",
                text: "El usuario ahora esta inactivo",
                icon: 'success'
              });
              path.style.display = "none";
            }
          },
          error => {
            Swal.fire({
              title: "Erro!",
              text: "El empleado no pudo ser eliminado",
              icon: "error"
            })
          }
        )
      } else {
        console.log("No eliminado")
      }
    });
  }


}
