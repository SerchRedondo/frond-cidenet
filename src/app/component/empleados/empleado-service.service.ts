import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable, pipe, of } from 'rxjs'
import { catchError } from 'rxjs/operators'
import { environment } from '../../../environments/environment';
import { Empleado } from '../../model/empleado';

@Injectable({
  providedIn: 'root'
})
export class EmpleadoServiceService {


  constructor(
    private http: HttpClient
  ) { }


  obtenerEmpleados(): Observable<any> {

    return this.http.get(environment.URL_EMPLEADO).
      pipe(
        catchError(this.handleError('obtenerEmpleados', []))
      )
  }


  guardarEmpleado(body: Empleado): Observable<any> {
    return this.http.post(environment.URL_EMPLEADO, body)

  }

  eliminarEmpleado(id: string): Observable<any> {
    return this.http.delete(environment.URL_EMPLEADO + `/${id}`)
  }

  obtenerUnEmpleado(id: string): Observable<any> {
    return this.http.get(environment.URL_EMPLEADO + `/${id}`)
  }

  actualizarEmpleado(id: string, empleado: any): Observable<any> {
    return this.http.put(environment.URL_EMPLEADO + `/${id}`, empleado)
  }


  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.log(error)

      console.log(`${operation} failed:${error.message}`);

      return of(result as T);
    }
  }

}
