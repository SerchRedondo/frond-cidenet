import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListEmpleadosComponent } from './component/empleados/list-empleados/list-empleados.component';
import { CreaEmpleadoComponent } from './component/empleados/crea-empleado/crea-empleado.component';
import { EditEmpleadoComponent } from './component/empleados/edit-empleado/edit-empleado.component';

const routes: Routes = [
  { path: '', component: ListEmpleadosComponent },
  { path: 'agregar', component: CreaEmpleadoComponent },
  { path: 'editar/:id', component: EditEmpleadoComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
