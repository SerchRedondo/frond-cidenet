import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'buscarPorCorreo'
})
export class BuscarPorCorreoPipe implements PipeTransform {

  transform(nombres: any, arg: any): any {

    const buscarpornombre = [];
    //console.log(arg)
    if (nombres) {
      //console.log(nombres);
      for (const recorrido of nombres) {
        // console.log(recorrido)
        // console.log(recorrido)
        // console.log(recorrido.p_nombre.toLowerCase().indexOf(arg.toLowerCase()) > -1)
        if (recorrido.correo.toLowerCase().indexOf(arg.toLowerCase()) > -1) {
          buscarpornombre.push(recorrido);
        }

      }
      ///console.log(buscarpornombre)
      return buscarpornombre;
    }
  }

}
