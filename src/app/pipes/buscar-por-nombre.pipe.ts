import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'buscarPorNombre'
})
export class BuscarPorNombrePipe implements PipeTransform {

  transform(nombres: any, arg: any): any {

    const buscarpornombre = [];
    //console.log(arg)
    if (nombres) {
      //console.log(nombres);
      for (const recorrido of nombres) {
        // console.log(recorrido)
        // console.log(recorrido)
        // console.log(recorrido.p_nombre.toLowerCase().indexOf(arg.toLowerCase()) > -1)
        if (recorrido.p_nombre.toLowerCase().indexOf(arg.toLowerCase()) > -1 ||
          recorrido.s_nombre.toLowerCase().indexOf(arg.toLowerCase()) > -1 ||
          recorrido.correo.toLowerCase().indexOf(arg.toLowerCase()) > -1 ||
          recorrido.n_identificacion.toString().indexOf(arg) > -1 ||
          recorrido.estado.toLowerCase().indexOf(arg.toLowerCase()) > -1 ||
          recorrido.pais.toLowerCase().indexOf(arg.toLowerCase()) > -1) {
          buscarpornombre.push(recorrido);
        }

      }
      ///console.log(buscarpornombre)
      return buscarpornombre;
    }
  }


}
