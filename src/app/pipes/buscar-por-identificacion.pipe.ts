import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'buscarPorIdentificacion'
})
export class BuscarPorIdentificacionPipe implements PipeTransform {

  transform(nombres: any, arg: any): any {

    const buscarpornombre = [];

    if (nombres) {
      //console.log(nombres);
      console.log(typeof arg)
      for (const recorrido of nombres) {

        // console.log(recorrido)

        //    console.log(recorrido.n_identificacion.toString().indexOf(arg.toLowerCase()) > -1)
        // console.log(typeof recorrido.n_identificacion.toString());
        // console.log(recorrido.n_identificacion.toString().indexOf(arg.toLowerCase()) > -1)
        if (recorrido.n_identificacion.indexOf(parseInt(arg)) > -1) {
          buscarpornombre.push(recorrido);
        }

      }
      ///console.log(buscarpornombre)
      return buscarpornombre;
    }
  }

}
