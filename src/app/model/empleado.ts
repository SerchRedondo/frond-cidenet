export interface Empleado {
  id?: string,
  p_apellido: string,
  s_apellido: string,
  p_nombre: string,
  s_nombre: string,
  estado?: string,
  pais:string,
  correo:string,
  t_identificacion:string,
  n_identificacion:number,
  f_ingreso:Date,
  area:string,
}
